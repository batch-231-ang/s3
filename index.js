console.log("Hello World");

class Dog{
	constructor(name, breed, age){
		this.name = name;
		this.breed = breed;
		this.age = age * 7;
	}
};

let dog1 = new Dog("Puchi", "Golden Retriever", 0.6);
console.log(dog1);



/*
	Activity #3:
	     1. Should class methods be included in the class constructor?
	        Answer: No, it should be added outside the constructor


	    2. Can class methods be separated by commas?
	        Answer: No, class methods does not have a separator


	    3. Can we update an object’s properties via dot notation?
	        Answer: Yes


	    4. What do you call the methods used to regulate access to an object’s properties?
	        Answer: Access Regulators


	    5. What does a method need to return in order for it to be chainable?
	        Answer: Object
*/


//mini-activity
class Person{
	constructor(name, age, nationality, address){
		this.name = name;
		
		if(age >= 18){
			this.age = age;
		}else{
			this.age = undefined;
		}
		
		this.nationality = nationality;
		this.address = address;
	}

	greet(){
		console.log(`Hello, Good Morning!`);
		return this;
	}

	introduce(){
		console.log(`Hi! My name is ${this.name}`);
		return this;
	}

	changeAddress(newAddress){
		this.address = newAddress;
		console.log(`${this.name} now lives in ${this.address}`);
		return this;
	}
};

let person1 = new Person("Rick Sanchez", 60, "American", "Florida");
let person2 = new Person("Morty", 17, "Filipino", "Los Angeles");
console.log(person1);
console.log(person2);

//mini-quiz
/*
	1. What is the blueprint where objects are created from?
	        Answer: classes


	    2. What is the naming convention applied to classes?
	        Answer: Uppercase for the beginning character


	    3. What keyword do we use to create objects from a class?
	        Answer: new


	    4. What is the technical term for creating an object from a class?
	        Answer: instantiation


	    5. What class method dictates HOW objects will be created from that class?
	    	Answer: constructor()
*/


class Student{
	constructor(name, email, grades){
		this.name = name;
		this.email = email;

		if(grades.length === 4){
			if(grades.every(grade => grade >= 0 && grade <= 100)){
				this.grades = grades;
			}else{
				this.grades = undefined;
			}
		}else{
				this.grades = undefined;
		}

		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;
	}

	login(){
		console.log(`${this.email} has logges in`);
		return this;
	}

	logout(email){
	    console.log(`${this.email} has logged out`);
	    return this;
	}

	listGrades(grades){
   	 this.grades.forEach(grade => {
        console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
    	})
   	 return this;
	}

	computeAve(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		this.gradeAve = sum / 4;
		return this;
	}

	willPass(){
		// you can call methods inside an object
		this.passed = this.gradeAve >= 85 ? true : false;
		return this;
	}

	willPassWithHonors(){
		if(this.gradeAve >= 90){
			this.passedWithHonors = true;
		}else{
			this.passedWithHonors = false;
		}
		return this;
	}

}

let studentOne = new Student("Tony", "starksindustries@mail.com", [-1, 84, 78, 88]);
let studentTwo = new Student("Peter", "spideyman@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Wanda", "scarlettMaximoff@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Steve", "captainRogers@mail.com", [91, 89, 92, 93]);
